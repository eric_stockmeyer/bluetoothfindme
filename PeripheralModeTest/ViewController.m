//
//  ViewController.m
//  PeripheralModeTest
//
//  Created by Eric Stockmeyer on 9/20/2012.
//  
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize Label;
@synthesize Log;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    manager = [[CBPeripheralManager alloc]initWithDelegate:self queue:nil];
    centmanager = [[CBCentralManager alloc]initWithDelegate:self queue:nil];
    connectedDevices =[[NSMutableDictionary alloc] init];
    connectedArray =[NSMutableArray array];
    
    //centralManager
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)centralManagerDidUpdateState:(CBCentralManager *)central{
    switch (central.state) {
        case CBCentralManagerStatePoweredOn:
            // Do we want the same device to be able to repeatedly connect
            [centmanager scanForPeripheralsWithServices:nil options:@{ CBCentralManagerScanOptionAllowDuplicatesKey : [NSNumber numberWithBool:YES] }];
            //[centmanager scanForPeripheralsWithServices:nil options:@{ CBCentralManagerScanOptionAllowDuplicatesKey : [NSNumber numberWithBool:NO] }];
            break;
            
        default:
            NSLog(@"%i",central.state);
            break;
    }
}

- (void) centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)aPeripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI {
    if ([RSSI floatValue]>=-60.f) {
    NSLog(@"Greater than -45");
        //[central stopScan];
        aCperipheral = aPeripheral;
        [central connectPeripheral:aCperipheral options:nil];
        NSString *localName = [advertisementData objectForKey:CBAdvertisementDataLocalNameKey];
        NSLog(@"Your Name Is: %@", localName);
        if(localName != nil)
        {
            Boolean shouldAddObject = true;
            NSMutableDictionary *info =[[NSMutableDictionary alloc] init];
            [info setObject:RSSI forKey:@"rssi"];
            [info setObject:localName forKey:@"name"];
            //[connectedDevices setObject:info forKey:localName];
            if(connectedArray.count >0)
            {
                for (id object in connectedArray) {
                    if([object objectForKey:@"name"]== aPeripheral)
                    {
                        shouldAddObject = false;
                        [object setObject:RSSI forKey:@"rssi"];
                        //[connectedArray addObject:info];
                    }
                    else
                    {
                        shouldAddObject = true;
                    }
                    // do something with object
                }
            }
            else
            {
               if(shouldAddObject == true)
               {
                [connectedArray addObject:info];
               }
            }
            
        }
        //[self.connectedDevices setObject:productID forKey:@"productID"];[fruit setObject:nameID forKey:@"nameID"];
        [self.peopleTAbleView reloadData];
        NSLog(@"Connected dvices: %@", connectedDevices);
        for(id key in connectedDevices)
        {
            NSLog(@"key: %@, value: %@ \n", key,[connectedDevices objectForKey:key]);
        }

    }
    
    

}

- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error{
    NSLog(@"Failed:%@",error);
}

- (void) centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)aPeripheral
{
    NSLog(@"Connected:%@",aPeripheral.UUID);
    [aCperipheral setDelegate:self];
    [aCperipheral discoverServices:nil];
}

- (void) peripheral:(CBPeripheral *)aPeripheral didDiscoverServices:(NSError *)error {
    for (CBService *aService in aPeripheral.services){
        if ([aService.UUID isEqual:[CBUUID UUIDWithString:@"EBA38950-0D9B-4DBA-B0DF-BC7196DD44FC"]]) {
            [aPeripheral discoverCharacteristics:nil forService:aService];
        }
    }
}

- (void) peripheral:(CBPeripheral *)aPeripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    for (CBCharacteristic *aChar in service.characteristics){
        NSLog(@"%@",aChar.UUID);
        
        if ([aChar.UUID isEqual:[CBUUID UUIDWithString:@"DA17"]]) {
            //NSLog(@"Find DA17");
            NSString *mainString = [NSString stringWithFormat:@"DA12312"];
            NSData *mainData1= [mainString dataUsingEncoding:NSUTF8StringEncoding];
            [aPeripheral writeValue:mainData1 forCharacteristic:aChar type:CBCharacteristicWriteWithResponse];
        }
    }
}

- (void)peripheral:(CBPeripheral *)aPeripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error{
    [centmanager cancelPeripheralConnection:aPeripheral];
}

- (void)peripheralManagerDidUpdateState:(CBPeripheralManager *)peripheral{
    NSLog(@"Done");
    switch (peripheral.state) {
        case CBPeripheralManagerStatePoweredOn:{
            CBUUID *cUDID = [CBUUID UUIDWithString:@"DA18"];
            CBUUID *cUDID1 = [CBUUID UUIDWithString:@"DA17"];
            CBUUID *cUDID2 = [CBUUID UUIDWithString:@"DA16"];
            
            
            CBUUID *sUDID = [CBUUID UUIDWithString:@"EBA38950-0D9B-4DBA-B0DF-BC7196DD44FC"];
            characteristic = [[CBMutableCharacteristic alloc]initWithType:cUDID properties:CBCharacteristicPropertyNotify value:nil permissions:CBAttributePermissionsReadable];
            characteristic1 = [[CBMutableCharacteristic alloc]initWithType:cUDID1 properties:CBCharacteristicPropertyWrite value:nil permissions:CBAttributePermissionsWriteable];
            characteristic2 = [[CBMutableCharacteristic alloc]initWithType:cUDID2 properties:CBCharacteristicPropertyRead value:nil permissions:CBAttributePermissionsReadable];
            NSLog(@"%u",characteristic2.properties);
            servicea = [[CBMutableService alloc]initWithType:sUDID primary:YES];
            servicea.characteristics = @[characteristic,characteristic1,characteristic2];
            [peripheral addService:servicea];
        }
            break;
            
        default:
            NSLog(@"%i",peripheral.state);
            break;
    }
}

- (void)peripheralManager:(CBPeripheralManager *)peripheral didAddService:(CBService *)service error:(NSError *)error{
    NSLog(@"Added");
    // Get the name of the device you are on
    NSString *myName = [[UIDevice currentDevice] name];
    // advertise with known CBUUID and your device name
    NSDictionary *advertisingData = @{CBAdvertisementDataLocalNameKey : myName, CBAdvertisementDataServiceUUIDsKey : @[[CBUUID UUIDWithString:@"EBA38950-0D9B-4DBA-B0DF-BC7196DD44FC"]]};
    NSLog(@"My Name Is: %@", myName);


    [peripheral startAdvertising:advertisingData];
}

- (void)peripheralManagerDidStartAdvertising:(CBPeripheralManager *)peripheral error:(NSError *)error{
    NSLog(@"Started Advertising");
}

- (void)peripheralManager:(CBPeripheralManager *)peripheral central:(CBCentral *)central didSubscribeToCharacteristic:(CBCharacteristic *)characteristic12{
    NSLog(@"Core:%@",characteristic12.UUID);
    NSLog(@"Connected");
    [self writeData:peripheral];
}

- (void)writeData:(CBPeripheralManager *)peripheral{
    NSDictionary *dict = @{ @"NAME" : @"Eric Stockmeyer",@"EMAIL":@"blblabla@gmail.com" };
    mainData = [NSJSONSerialization dataWithJSONObject:dict options:kNilOptions error:nil];
    while ([self hasData]) {
        if([peripheral updateValue:[self getNextData] forCharacteristic:characteristic onSubscribedCentrals:nil]){
            [self ridData];
        }else{
            return;
        }
    }
    NSString *stra = @"ENDAL";
    NSData *dataa = [stra dataUsingEncoding:NSUTF8StringEncoding];
    [peripheral updateValue:dataa forCharacteristic:characteristic onSubscribedCentrals:nil];
}

- (void)peripheralManagerIsReadyToUpdateSubscribers:(CBPeripheralManager *)peripheral{
    while ([self hasData]) {
        if([peripheral updateValue:[self getNextData] forCharacteristic:characteristic onSubscribedCentrals:nil]){
            [self ridData];
        }else{
            return;
        }
    }
    NSString *stra = @"ENDAL";
    NSData *dataa = [stra dataUsingEncoding:NSUTF8StringEncoding];
    [peripheral updateValue:dataa forCharacteristic:characteristic onSubscribedCentrals:nil];
}

- (BOOL)hasData{
    if ([mainData length]>0) {
        return YES;
    }else{
        return NO;
    }
}

- (void)ridData{
    if ([mainData length]>19) {
        mainData = [mainData subdataWithRange:NSRangeFromString(range)];
    }else{
        mainData = nil;
    }
}

- (NSData *)getNextData
{
    NSData *data;
    if ([mainData length]>19) {
        int datarest = [mainData length]-20;
        data = [mainData subdataWithRange:NSRangeFromString(@"{0,20}")];
        range = [NSString stringWithFormat:@"{20,%i}",datarest];
    }else{
        int datarest = [mainData length];
        range = [NSString stringWithFormat:@"{0,%i}",datarest];
        data = [mainData subdataWithRange:NSRangeFromString(range)];
    }
    return data;
}

- (void)peripheralManager:(CBPeripheralManager *)peripheral didReceiveReadRequest:(CBATTRequest *)request{
    NSString *mainString = [NSString stringWithFormat:@"GN123"];
    NSData *cmainData= [mainString dataUsingEncoding:NSUTF8StringEncoding];
    request.value = cmainData;
    [peripheral respondToRequest:request withResult:CBATTErrorSuccess];
}

- (void)peripheralManager:(CBPeripheralManager *)peripheral didReceiveWriteRequests:(NSArray *)requests{
    for (CBATTRequest *aReq in requests){
        //NSLog(@"%@",[[NSString alloc]initWithData:aReq.value encoding:NSUTF8StringEncoding]);
        Log.text = [Log.text stringByAppendingString:[[NSString alloc]initWithData:aReq.value encoding:NSUTF8StringEncoding]];
        Log.text = [Log.text stringByAppendingString:@"\n"];
        [peripheral respondToRequest:aReq withResult:CBATTErrorSuccess];
    }
}

- (void)willEnterBackgroud{
    [manager stopAdvertising];
    [centmanager stopScan];
}

- (void)willBacktoForeground{
    NSDictionary *advertisingData = @{CBAdvertisementDataServiceUUIDsKey : @[[CBUUID UUIDWithString:@"EBA38950-0D9B-4DBA-B0DF-BC7196DD44FC"]]};
    [manager startAdvertising:advertisingData];
    [centmanager scanForPeripheralsWithServices:nil options:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    //return [catagorry count];    //count number of row from counting array hear cataGorry is An Array
    //return 3;
    return [connectedArray count];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"personCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    //NSArray *keys = [connectedDevices allKeys];
    //cell.textLabel.text = [tableData objectAtIndex:indexPath.row];
    //cell.textLabel.text = [connectedDevices objectForKey:[keys objectAtIndex:indexPath.row]];
    cell.textLabel.text = [[connectedArray objectAtIndex:indexPath.row] objectForKey: @"name"];
    
    
    

    // = [[connectedArray objectAtIndex:indexPath.row] objectForKey: @"name"];



    //cell.textLabel.text = @"test";
    
    return cell;
}




@end
