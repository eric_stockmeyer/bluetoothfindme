//
//  ViewController.h
//  PeripheralModeTest
//
//  Created by Eric Stockmeyer on 9/20/2012.
//  
//

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>

@interface ViewController : UIViewController<CBPeripheralManagerDelegate,CBCentralManagerDelegate,CBPeripheralDelegate, UITableViewDataSource, UITableViewDelegate>{
    CBPeripheralManager *manager;
    CBCentralManager *centmanager;
    CBMutableCharacteristic *characteristic;
    CBMutableCharacteristic *characteristic1;
    CBMutableCharacteristic *characteristic2;
    CBMutableService *servicea;
    NSData *mainData;
    NSString *range;
    NSMutableDictionary  *connectedDevices;
    NSMutableArray *connectedArray;
    //NSMutableDictionary *connectedDevices;
    
    CBPeripheral *aCperipheral;
}
@property (weak, nonatomic) IBOutlet UILabel *Label;
@property (weak, nonatomic) IBOutlet UITextView *Log;
@property (weak, nonatomic) IBOutlet UITableView *peopleTAbleView;


- (void)willEnterBackgroud;
- (void)willBacktoForeground;

@end
