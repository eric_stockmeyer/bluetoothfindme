//
//  AppDelegate.h
//  PeripheralModeTest
//
//  Created by Eric Stockmeyer on 9/20/2012.
//  
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
