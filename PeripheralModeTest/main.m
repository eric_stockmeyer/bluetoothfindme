//
//  main.m
//  PeripheralModeTest
//
//  Created by Eric Stockmeyer on 9/20/2012.
//  
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
